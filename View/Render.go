package Render

import (
	"log"
	"net/http"
	"text/template"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("templates/*"))
}
func RenderView(w http.ResponseWriter, html string, data interface{}) error {
	//go get html file
	err := tpl.ExecuteTemplate(w, html, data)
	if err != nil {
		log.Fatalln("template didn't execute: ", err)
	}
	return err

}
