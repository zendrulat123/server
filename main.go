package main

import (
	"fmt"
	"log"
	"net/http"

	Render "./View"
)

//used to get template stuff

var err error

//main function
func main() {

	//actual server
	mux := http.NewServeMux()
	mux.HandleFunc("/go", Gohome)

	//used to get other files css/js
	fs := http.FileServer(http.Dir("dailyplanetnews/"))
	http.Handle("/dailyplanetnews/", http.StripPrefix("/dailyplanetnews/", fs))
	log.Fatal(http.ListenAndServe(":8082", nil))
}

//only one
func Gohome(w http.ResponseWriter, r *http.Request) {

	//switch statement for get or post
	switch r.Method {

	case "GET":
		err := Render.RenderView(w, "index.html", nil)
		if err != nil {
			log.Fatalln("template didn't execute: ", err)
		}
	// case "GET":
	// 	//go get html file
	// 	err := tpl.ExecuteTemplate(w, "index.html", nil)
	// 	if err != nil {
	// 		log.Fatalln("template didn't execute: ", err)
	// 	}

	case "POST":
		Render.RenderView(w, "index.html", nil)
		if err != nil {
			log.Fatalln("template didn't execute: ", err)
		}
	default:
		fmt.Fprintf(w, "Sorry, only GET and POST methods are supported.")
	}

}
